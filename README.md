
This is an example API that I extracted and opened to use as a target for HipChat slash commands.

## deploying to Heroku

```bash
heroku create pypples
git push heroku master
heroku ps:scale web=1
```
## Licensing

This is licensed under Apache 2.0, see LICENSE.txt for more details.
